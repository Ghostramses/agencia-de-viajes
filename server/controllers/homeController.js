//Importar el modelo de los viajes
const Viaje = require('./../models/Viajes');
//Importar el modelo de los testimoniales
const Testimonial = require('./../models/Testimoniales');

exports.consultasHomePage = async (req, resp) => {
	// Para realizar muchos promises al mismo tiempo utilizar async await

	const viajes = await Viaje.findAll({
		limit: 3
	});

	const testimoniales = await Testimonial.findAll({
		limit: 3
	});

	resp.render('index', {
		clase: 'home',
		viajes,
		testimoniales
	});
};
