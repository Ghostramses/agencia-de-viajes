//Importar el modelo de los testimoniales
const Testimonial = require('./../models/Testimoniales');

exports.mostrarTestimoniales = async (req, res) => {
	const testimoniales = await Testimonial.findAll();
	res.render('testimoniales', {
		pagina: 'Testimoniales',
		testimoniales
	});
};

exports.agregarTestimonial = async (req, res) => {
	//Validar que los campos estén llenos
	let { nombre, correo, mensaje } = req.body;

	let errores = [];
	if (!nombre) {
		errores.push({ mensaje: 'Agrega tu Nombre' });
	}
	if (!correo) {
		errores.push({ mensaje: 'Agrega tu Correo' });
	}
	if (!mensaje) {
		errores.push({ mensaje: 'Agrega tu Mensaje' });
	}

	//Revisar si existen errores
	if (errores.length > 0) {
		const testimoniales = await Testimonial.findAll();
		//Muestra la vista con errores
		res.render('testimoniales', {
			pagina: 'Testimoniales',
			errores,
			nombre,
			correo,
			mensaje,
			testimoniales
		});
	} else {
		await Testimonial.create({
			nombre,
			correo,
			mensaje
		});

		res.redirect('/testimoniales');
	}
};
