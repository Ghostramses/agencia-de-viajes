//Importar express
const express = require('express');
//Importar las rutas
const routes = require('./routes');
//Importar path (ficheros del sistema)
const path = require('path');
//Importar el body parser para manejar las peticiones
const bodyParser = require('body-parser');

//Importar configuraciones
const config = require('./config');

//Importar dotenv
require('dotenv').config({ path: 'variables.env' });

//Configurar express
const app = express();

//Habilitar el motor de plantillas (pug)
app.set('view engine', 'pug');

//Añadir las vistas
app.set('views', path.join(__dirname, 'views'));

//Añadir los archivos estaticos
app.use(express.static('public'));

//Validar si estamos en desarrollo o en produccion
const conf = config[app.get('env')];

//Se coloca el nombre del sitio en una variable de entorno despues de validar el entorno
app.locals.titulo = conf.nombreSitio;

console.log(app.get('env'));

//Guarda la fecha actual en una variable de entorno
//Guarda la ruta donde se encuentra el usuario
app.use((req, res, next) => {
	const fecha = new Date();
	//Asignacion a la variable de entorno
	res.locals.fechaActual = fecha.getFullYear();

	//Crear una variable nueva para indicar al usuario donde se encuentra
	res.locals.ruta = req.path;

	//Se retorna next para continuar con la siguiente funcion
	return next();
});

//Ejecutar el body parser
app.use(bodyParser.urlencoded({ extended: true }));

//Cargar las rutas
app.use('/', routes());

// * Puerto y host para la app

const host = process.env.HOST || '0.0.0.0';
const port = process.env.PORT || 3000;

//Encender el server
app.listen(port, host, () => console.log('El servidor está funcionando'));
