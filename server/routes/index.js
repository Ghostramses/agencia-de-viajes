const express = require('express');

//Crear un router
const router = express.Router();

// * Importar los controladores
//Controlador del home
const homeController = require('./../controllers/homeController');
//Controlador de nosotros
const nosotrosController = require('./../controllers/nosotrosController');
//Controlador de los viajes
const viajesController = require('./../controllers/viajesController');
//Controlador de los testimoniales
const testimonialesController = require('./../controllers/testimonialesController');

module.exports = () => {
	router.get('/', homeController.consultasHomePage);
	router.get('/nosotros', nosotrosController.infoNosotros);
	router.get('/viajes', viajesController.mostrarViajes);
	router.get('/viajes/:id', viajesController.mostrarViaje);
	router.get('/testimoniales', testimonialesController.mostrarTestimoniales);

	//Cuando se llene el formulario de los testimoniales
	router.post('/testimoniales', testimonialesController.agregarTestimonial);

	return router;
};
