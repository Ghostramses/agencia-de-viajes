//Importar sequelize
const sequelize = require('sequelize');
//Importar la configuracion de la base de datos
const db = require('../config/database');

//Definir el modelo
const Testimoniales = db.define('testimoniales', {
	idTestimoniales: {
		type: sequelize.INTEGER,
		primaryKey: true
	},
	nombre: {
		type: sequelize.STRING(60)
	},
	correo: {
		type: sequelize.STRING(30)
	},
	mensaje: {
		type: sequelize.STRING
	}
});

module.exports = Testimoniales;
