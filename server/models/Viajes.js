//Importar sequelize
const Sequelize = require('sequelize');
const sequelize = require('sequelize');

// Importar la configuracion de la base de datos
const db = require('./../config/database');

const Viaje = db.define('viaje', {
	idViajes: {
		type: sequelize.INTEGER,
		primaryKey: true
	},
	titulo: {
		type: sequelize.STRING(60)
	},
	precio: {
		type: sequelize.STRING(10)
	},
	fechaIda: {
		type: sequelize.DATE
	},
	fechaVuelta: {
		type: sequelize.DATE
	},
	imagen: {
		type: sequelize.STRING(20)
	},
	descripcion: {
		type: sequelize.STRING
	},
	lugaresDisponibles: {
		type: sequelize.STRING(3)
	}
});

module.exports = Viaje;
