# Portal web de una agencia de viajes

## Dependencias

-   Express
-   Pug
-   Sequelize
-   MySQL

## Requisitos

### Crear un archivo de nombre: "variables.env" en la raíz del proyecto con las variables:

-   BD_NOMBRE
-   BD_USER
-   BD_PASS
-   BD_HOST
-   BD_PORT
-   HOST
